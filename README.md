# Weather Clothing Recommendation App
This Flask application provides clothing recommendations based on the current weather. It uses the OpenWeatherMap API to fetch current weather data, which includes temperature and weather conditions. The application then provides a clothing recommendation based on the temperature, weather conditions, and season.

# Installation

Before you start, ensure you have Python and pip installed on your computer.

1. Clone the repository to your local machine:

```
git clone https://gitlab.com/a1555018/u06.git

```

 

2. Navigate to the u06 folder:

```
cd u06

```

3. Install the required Python packages:

```
pip install -r requirements.txt
```

# Usage

1. Run the Flask application:

```
python api.py

```

2. Open a web browser and navigate to http://localhost:5000/recommendation/<city> where <city> is the name of the city for which you want clothing recommendations. For example, to get recommendations for London, go to http://localhost:5000/recommendation/London.


3. Run the cli application

```
python cli.py 'City'

```
Enter a city of your choice to get the temperature information and the recommended clothing for that city.

# Overview of Key Components

- `WeatherSession` : A class that handles communication with the OpenWeatherMap API.

- `get_weather` : A method in `WeatherSession` that fetches the current weather for a given city.

- `recommend_clothing` : A method in WeatherSession that provides a clothing recommendation based on temperature.

- `get_clothing_recommendations`: A method in WeatherSession that provides a clothing recommendation based on temperature, weather conditions, and season.

# API Key
To use the OpenWeatherMap API, you need an API key. You can obtain one by signing up on the OpenWeatherMap website. Replace 'your_api_key' in the code with your actual API key.


# Caution
The app uses a simple approximation for determining the season based on the current month and assumes the Northern Hemisphere. In reality, the start and end dates of the seasons can vary slightly each year and are different in the Southern Hemisphere. More complex methods may be required for accurate season determination, especially for locations near the equator or in the Southern Hemisphere..


# Contribution
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change. Please make sure to update tests as appropriate.




