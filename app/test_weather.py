import unittest
from weather import WeatherSession

class TestWeatherSession(unittest.TestCase):
    def setUp(self):
        self.ws = WeatherSession('bd1bcab249ad1db247ebaf7e069f596f')

    def test_get_weather(self):
        # call the method
        temperature, humidity, condition, date, day = self.ws.get_weather('London')

        # test the results
        self.assertIsInstance(temperature, float)
        self.assertIsInstance(humidity, (int, float))
        self.assertIsInstance(condition, str)
        self.assertIsInstance(date, str)
        self.assertIsInstance(day, str)

    def test_get_season(self):
        season = self.ws.get_season()
        self.assertIn(season.lower(), [s.lower() for s in ['Spring', 'Summer', 'Autumn', 'Winter']])

    # Add more tests for each method in your WeatherSession class

if __name__ == '__main__':
    unittest.main()
