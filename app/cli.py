import argparse
from weather import WeatherSession

def main():
    parser = argparse.ArgumentParser(description='Get clothing recommendations for a city.')
    parser.add_argument('city', type=str, help='The city to get recommendations for')

    args = parser.parse_args()

    api_key = "bd1bcab249ad1db247ebaf7e069f596f"  # Replace with your OpenWeatherMap API key

    ws = WeatherSession(api_key)  # Replace with your OpenWeatherMap API key

    season = ws.get_season()
    temperature, humidity, condition, date, day = ws.get_weather(args.city)
    recommendation = ws.get_clothing_recommendations(condition, season)

    print(f"In {args.city}, Temperature is {temperature}. It is recommended to wear: {recommendation}")

if __name__ == "__main__":
    main()
