FROM python:3.10-alpine
WORKDIR /app
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY api.py .
COPY cli.py .

CMD ["python", "api.py"]
